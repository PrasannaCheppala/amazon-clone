import React from 'react';
import './Home.css'
import Product from './Product.js';

function Home(){
  return (
    <div className = 'home'>
      <div className = 'home__container'>
        <img className='home__image' src = "  https://assets.ajio.com/medias/sys_master/images/images/hab/h6b/34371082649630/05082021-d-unisex-topbanner-ajiomania-50to90-extra30.jpg" alt=""/>
      
      <div className = 'home__row'>
      
        <Product 
                  id = '123456'
                  title = "Rich Dad Poor Dad: What the Rich Teach Their Kids About Money That the Poor and Middle Class Do Not! "
                  price = {29.99}
                  image = 'https://images-eu.ssl-images-amazon.com/images/I/91VokXkn8hL._AC_UL480_SR300,480_.jpg'
                  rating = {5}
                  />
        <Product 
                  id = '123456'
                  title = "Butterfly Rhino Plus Wet Grinder, 2L (Grey) , 150W"
                  price = {29.99}
                  image = 'https://m.media-amazon.com/images/I/71WaBDJdTIL._AC_UL480_FMwebp_QL65_.jpg'
                  rating = {5}
                  />
       
      
      </div>

      <div className = 'home__row'>
      <Product 
                  id = '123456'
                  title = "Mi Smart Band 5 – India’s No. 1 Fitness Band, 1.1-inch AMOLED Color Display"
                  price = {29.99}
                  image = 'https://images-eu.ssl-images-amazon.com/images/I/41TXCe7NGML._AC_SX184_.jpg'
                  rating = {5}
                  />
         <Product 
                  id = '123778'
                  title = "(Renewed) Portronics POR-871 SoundDrum 4.2 Stereo Speaker (Black)"
                  price = {98.99}
                  image = 'https://m.media-amazon.com/images/I/51nqNv80wFL._AC_SY200_.jpg'
                  rating = {5}
         />
        <Product 
                  
                  id = '123573'
                  title = "OnePlus 9R 5G (Carbon Black, 8GB RAM, 128GB Storage)"
                  price = {29.99}
                  image = "https://m.media-amazon.com/images/I/71KVeQql77S._AC_UL480_FMwebp_QL65_.jpg"
                  rating = {5}
         />
       
      </div>

      <div className = 'home__row'>
      <Product 
                  id = '123573'
                  title = "TCL 107.86 cm (43 inches) Full HD LED Certified Android Smart TV P30 43P30FS (Black)"
                  price = {29.99}
                  image = "https://m.media-amazon.com/images/I/81SQ3nSk7rL._AC_UL480_FMwebp_QL65_.jpg"
                  rating = {5}
         />
     
      </div>
      
      </div>

    </div>
  );
}
export default Home;